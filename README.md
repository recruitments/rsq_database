# RSQ database

## Set up database

1. Download docker and docker-compose
2. Go into directory to which you wanna download project
3. `git clone https://gitlab.com/recruitments/rsq_database.git`
4. cd ./rsq_database
5. docker-compose up --build -d

You can now view your database by link: http://localhost:5998. Credentials are inside .env file.

