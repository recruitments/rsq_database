#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL

  CREATE USER "$POSTGRES_DB_USER" WITH PASSWORD '$POSTGRES_DB_PASSWORD';

  CREATE SEQUENCE IF NOT EXISTS specializations_id_seq;

  CREATE TABLE IF NOT EXISTS specializations(
    specialization_id INTEGER PRIMARY KEY DEFAULT NEXTVAL('specializations_id_seq'),
    name VARCHAR(256) NOT NULL
  );

  CREATE SEQUENCE IF NOT EXISTS doctors_id_seq;

  CREATE TABLE IF NOT EXISTS doctors (
    doctor_id BIGINT PRIMARY KEY DEFAULT NEXTVAL('doctors_id_seq'),
    first_name VARCHAR(256) NOT NULL,
    last_name VARCHAR(256) NOT NULL,
    specialization_id INTEGER REFERENCES specializations(specialization_id)
  );

  CREATE SEQUENCE IF NOT EXISTS patients_id_seq;

  CREATE TABLE IF NOT EXISTS patients(
    patient_id BIGINT PRIMARY KEY DEFAULT NEXTVAL('patients_id_seq'),
    first_name VARCHAR(256) NOT NULL,
    last_name VARCHAR(256) NOT NULL,
    address VARCHAR(256)
  );

  CREATE SEQUENCE IF NOT EXISTS appointment_id_seq;

  CREATE TABLE IF NOT EXISTS appointments(
    appointment_id BIGINT PRIMARY KEY NOT NULL DEFAULT NEXTVAL('appointment_id_seq'),
    patient_id BIGINT REFERENCES patients(patient_id) NOT NULL,
    doctor_id BIGINT REFERENCES doctors(doctor_id) NOT NULL,
    date date NOT NULL,
    time time NOT NULL,
    address VARCHAR(256) NOT NULL
  );

  CREATE SEQUENCE IF NOT EXISTS translations_id_seq;

  CREATE TABLE IF NOT EXISTS translations(
    translation_name VARCHAR(256) PRIMARY KEY DEFAULT NEXTVAL('translations_id_seq'),
    pl VARCHAR(2048) NOT NULL,
    en VARCHAR(2048) NOT NULL
  );

  INSERT INTO translations VALUES ('DENTIST', 'Dentysta', 'Dentist') ON CONFLICT DO NOTHING;
  INSERT INTO translations VALUES ('PHYSIOTHERAPIST', 'Fizjoterapeuta', 'Physiotherapist') ON CONFLICT DO NOTHING;

  INSERT INTO specializations(name) VALUES ('DENTIST') ON CONFLICT DO NOTHING;
  INSERT INTO specializations(name) VALUES ('PHYSIOTHERAPIST') ON CONFLICT DO NOTHING;

  INSERT INTO doctors(first_name, last_name, specialization_id) VALUES('Jan', 'Kowalski', 1) ON CONFLICT DO NOTHING;
  INSERT INTO doctors(first_name, last_name, specialization_id) VALUES('Krzysztof', 'Nowak', 2) ON CONFLICT DO NOTHING;

  INSERT INTO patients(first_name, last_name, address) VALUES ('Marzena', 'Kowalczyk', null) ON CONFLICT DO NOTHING;

  INSERT INTO appointments(patient_id, doctor_id, date, time, address) VALUES (1, 1, now(), '15:00:00', '3 maja') ON CONFLICT DO NOTHING;

  GRANT ALL PRIVILEGES ON DATABASE "$POSTGRES_DB" TO "$POSTGRES_DB_USER";
  GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO "$POSTGRES_DB_USER";
  GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO "$POSTGRES_DB_USER";


EOSQL
